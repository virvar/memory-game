(ns memory-game.core
  (:require [clojure.walk :refer [prewalk]]
            [reagent.core :as r]))

(def board-size 4)
(def initial-cards
  (mapv (fn [index]
          {:type  (quot index 2)
           :state :closed})
        (range (* board-size board-size))))

(defonce app-state (r/atom {:title "Memory Game"}))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LOGIC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn game-over? [state]
  (->> (:board state)
       (apply concat)
       (every? (fn [card]
                 (= :open (:state card))))))

(defn get-clicked-cards [state]
  (->> (:board state)
       (apply concat)
       (filter #(= :clicked (:state %)))))

(defn match? [state]
  (let [clicked-cards (get-clicked-cards state)]
    (= (:type (first clicked-cards)) (:type (second clicked-cards)))))

(defn stop-not-match-animation [state]
  (if-let [timeout (:not-match-timeout state)]
    (do (js/clearTimeout timeout)
        (-> state
            (update :board #(prewalk (fn [card]
                                       (if (= :clicked (:state card))
                                         (assoc card :state :closed)
                                         card)) %))
            (assoc :not-match-timeout nil)))
    state))

(defn handle-first-click [state row col]
  (assoc-in state [:board row col :state] :clicked))

(defn handle-match [state]
  (update state :board #(prewalk (fn [card]
                                   (if (= :clicked (:state card))
                                     (assoc card :state :open)
                                     card)) %)))

(defn handle-not-match [state]
  (assoc state :not-match-timeout (js/setTimeout (fn []
                                                   (swap! app-state stop-not-match-animation))
                                                 1000)))

(defn handle-second-click [state row col]
  (let [new-state (assoc-in state [:board row col :state] :clicked)]
    (if (match? new-state)
      (handle-match new-state)
      (handle-not-match new-state))))

(defn on-card-click [state row col]
  (let [new-state (stop-not-match-animation state)]
    (if (empty? (get-clicked-cards new-state))
      (handle-first-click new-state row col)
      (handle-second-click new-state row col))))

(defn on-card-click! [row col]
  (swap! app-state on-card-click row col))

(defn generate-board []
  (->> initial-cards
       shuffle
       (partition-all board-size)
       (mapv vec)))

(defn start-game [state]
  (assoc state :board (generate-board)
               :not-match-timeout nil))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VIEWS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn closed-card [row col]
  [:div.card.closed-card
   {:on-click #(on-card-click! row col)}])

(defn open-card [card]
  [:div.card {:class (str "card" (:type card))}])

(defn cards-row [row index]
  [:div.row
   (for [n (range (count row))
         :let [card (nth row n)]]
     (if (or (= :open (:state card)) (= :clicked (:state card)))
       ^{:key n} [open-card card]
       ^{:key n} [closed-card index n]))])

(defn board-view [board]
  [:div
   (for [n (range (count board))
         :let [row (nth board n)]]
     ^{:key n} [cards-row row n])])

(defn page []
  (let [state @app-state]
    [:div
     [:p (:title state)]
     (if (game-over? state)
       [:p "Game over"])
     [board-view (:board state)]
     [:br]
     [:button {:on-click #(swap! app-state start-game)}
      "New game"]]))

(defn reload []
  (r/render [page]
            (.getElementById js/document "app")))

(defn ^:export main []
  (swap! app-state start-game)
  (reload))

;;; helpers
#_(defn update-card [state row col card-state]
    (assoc-in state [:board row col :state] card-state))
#_(swap! app-state update-card 0 0 :closed)
#_(swap! app-state update :board (fn [board]
                                   (prewalk (fn [item]
                                              (if (and (map? item) (not= 0 (:type item)))
                                                (assoc item :state :open)
                                                item)) board)))
